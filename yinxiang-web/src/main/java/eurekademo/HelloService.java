package eurekademo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HelloService {

//	//方式一：使用restTemplate
//    @Autowired
//    RestTemplate restTemplate;
//
//    public String hiService(String name) {
//        return restTemplate.getForObject("http://SERVICE-HI/hi?name="+name,String.class);
//    }
    
    //方式二：使用Feign
    @Autowired
    HiClientApi hiClientApi;
    
    public String hiService(String name) {
        return hiClientApi.hiService(name);
    }

}