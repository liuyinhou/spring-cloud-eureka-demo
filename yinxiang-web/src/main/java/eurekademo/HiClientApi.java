package eurekademo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "YINXIANG-SERVICE")
public interface HiClientApi {

    @GetMapping("/hi")
    public String hiService(@RequestParam("name") String name);
}