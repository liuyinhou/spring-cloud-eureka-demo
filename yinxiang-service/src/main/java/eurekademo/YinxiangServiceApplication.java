package eurekademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 *
 * @author Gunnar Hillert
 *
 */
@SpringBootApplication
@EnableEurekaClient
public class YinxiangServiceApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(YinxiangServiceApplication.class, args);
	}

}
